#ifndef KOICXX_THREAD_POOL_HPP
#define KOICXX_THREAD_POOL_HPP

#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include <memory>

namespace koicxx {

/**
 * \brief Simple implementation of the thread pool
 */
class thread_pool
{
public:
  void init(int pool_size)
  {
    _p_work.reset(new boost::asio::io_service::work(_io_service));

    for (int i = 0; i < pool_size; ++i)
    {
      _thread_group.create_thread(boost::bind(&boost::asio::io_service::run, &_io_service));                             
    }

    _working = true;
  }

  ~thread_pool()
  {
    boost::mutex::scoped_lock lock(_sync);

    _io_service.stop();
    _thread_group.join_all();
    _p_work.reset();

    _working = false;
  }

  template <typename F>
  void enqueue_task(F task)
  {
    boost::mutex::scoped_lock lock(_sync);

    if (_working)
    {
      _io_service.post(task);           
    }
  }

private:
  bool _working;
  boost::mutex _sync;
  boost::asio::io_service _io_service;
  std::unique_ptr<boost::asio::io_service::work> _p_work;
  boost::thread_group _thread_group;
};

} // namespace koicxx

#endif // !KOICXX_THREAD_POOL_HPP