#ifndef KOICXX_DEBUG_MEM_LEAKS_HPP
#define KOICXX_DEBUG_MEM_LEAKS_HPP

# ifndef _MSC_VER
#  error There's MSVC implementation of memory leaks detector atm
# endif // !_MSC_VER

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

# ifdef _DEBUG
#  ifndef DBG_NEW
#    define DBG_NEW new(_NORMAL_BLOCK , __FILE__ , __LINE__)
#    define new DBG_NEW
#  endif // !DBG_NEW
# endif // !_DEBUG

namespace koicxx {
namespace debug {

enum class mem_leaks_report_types { WARN, ERR, ASSERT };

namespace detail {

int get_internal_report_type(mem_leaks_report_types report_type)
{
  if (report_type == mem_leaks_report_types::WARN)
  {
    return _CRT_WARN;
  }
  else if (report_type == mem_leaks_report_types::ERR)
  {
    return _CRT_ERROR;
  }
  else if (report_type == mem_leaks_report_types::ASSERT)
  {
    return _CRT_ASSERT;
  }
}

} // namespace detail

void enable_mem_leaks_detector()
{
  _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
}

void redirect_mem_leaks_detector_output_to_debug_message_window(mem_leaks_report_types report_type)
{
  int internal_report_type = detail::get_internal_report_type(report_type);
  _CrtSetReportMode(internal_report_type, _CRTDBG_MODE_DEBUG);
}

void redirect_mem_leaks_detector_output_to_stderr(mem_leaks_report_types report_type)
{
  int internal_report_type = detail::get_internal_report_type(report_type);
  _CrtSetReportMode(internal_report_type, _CRTDBG_MODE_FILE);
  _CrtSetReportFile(internal_report_type, _CRTDBG_FILE_STDERR);
}

void redirect_mem_leaks_detector_output_to_stdout(mem_leaks_report_types report_type)
{
  int internal_report_type = detail::get_internal_report_type(report_type);
  _CrtSetReportMode(internal_report_type, _CRTDBG_MODE_FILE);
  _CrtSetReportFile(internal_report_type, _CRTDBG_FILE_STDOUT);
}

void redirect_mem_leaks_detector_output_to_window(mem_leaks_report_types report_type)
{
  int internal_report_type = detail::get_internal_report_type(report_type);
  _CrtSetReportMode(internal_report_type, _CRTDBG_MODE_WNDW);
}

} // namespace debug
} // namespace koicxx

#endif // !KOICXX_DEBUG_MEM_LEAKS_HPP