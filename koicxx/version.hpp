#ifndef KOICXX_VERSION_HPP
#define KOICXX_VERSION_HPP

/**
 * KOICXX_VERSION macro has the following format:
 * XXYYZZ
 * where XX is the major version,
 * YY -- minor version
 * and ZZ -- patch version
 * Leading zeroes are omitted
 *
 * Examples:
 * v0.1.0 -- 100
 * v0.1.5 -- 105
 * v0.1.15 -- 115
 * v0.15.6 -- 1506
 * v1.0.0 -- 100000
 */
#define KOICXX_VERSION 100

#define KOICXX_MAJOR_VERSION KOICXX_VERSION / 100000
#define KOICXX_MINOR_VERSION KOICXX_VERSION / 100 % 1000
#define KOICXX_PATCH_VERSION KOICXX_VERSION % 100

#endif // !KOICXX_VERSION_HPP