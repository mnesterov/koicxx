#ifndef KOICXX_TEMP_STORAGE_HPP
#define KOICXX_TEMP_STORAGE_HPP

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time.hpp>
#include <boost/function.hpp>
#include <boost/noncopyable.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/thread.hpp>

#include <map>
#include <utility>

namespace koicxx {

template <typename T>
class temp_storage : private boost::noncopyable
{
  typedef boost::shared_ptr<boost::asio::deadline_timer>                    shared_timer_t;
  typedef std::map<T, shared_timer_t>                                       timer_map_t;
  typedef std::pair<T, shared_timer_t>                                      timer_pair_t;
  typedef boost::function<void(const T&, const boost::system::error_code&)> callback_t;

public:
  temp_storage()
    : _do_poll_th(&temp_storage::do_poll, this) {}

  ~temp_storage()
  {
    _do_poll_th.interrupt();
    if (_do_poll_th.joinable())
    {
      _do_poll_th.join();
    }
  }

  bool add(const T& element, const boost::asio::deadline_timer::duration_type& timeout, callback_t callback = callback_t())
  {
    boost::lock_guard<boost::mutex> lock(_sync);

    const std::pair<timer_map_t::iterator, bool>& res =
      _internal_storage.insert(
        timer_pair_t(
          element
          , shared_timer_t(new boost::asio::deadline_timer(_io_service, timeout))
        ));

    if (!res.second)
    {
      return false;
    }

    const timer_map_t::iterator& itr = res.first;

    if (callback)
    {
      itr->second->async_wait(
        boost::bind(
          callback
          , itr->first
          , boost::asio::placeholders::error
        ));
    }

    itr->second->async_wait(
      boost::bind(
        &temp_storage::remove_callback
        , this
        , itr->first
        , boost::asio::placeholders::error
      ));

    return true;
  }

  bool remove(const T& element)
  {
    boost::lock_guard<boost::mutex> lock(_sync);

    const timer_map_t::iterator& itr = _internal_storage.find(element);
    if (itr == _internal_storage.end())
    {
      return false;
    }
    itr->second->cancel();

    _internal_storage.erase(itr);

    return true;
  }

  bool contains(const T& element)
  {
    boost::lock_guard<boost::mutex> lock(_sync);

    return _internal_storage.find(element) != _internal_storage.end();
  }

  void clear()
  {
    boost::lock_guard<boost::mutex> lock(_sync);

    for (timer_map_t::value_type& i : _internal_storage)
    {
      i.second->cancel();
    }

    _internal_storage.clear();
  }

private:
  void remove_callback(const T& element, const boost::system::error_code& e)
  {
    if (e == boost::asio::error::operation_aborted)
    {
      return;
    }
    remove(element);
  }

  void do_poll()
  {
    while (true)
    {
      _io_service.poll();
      boost::this_thread::yield();
      boost::this_thread::interruption_point();
    }
  }

  boost::asio::io_service _io_service;
  boost::thread           _do_poll_th;
  timer_map_t             _internal_storage;
  boost::mutex            _sync;
};

} // namespace koicxx

#endif // !KOICXX_TEMP_STORAGE_HPP