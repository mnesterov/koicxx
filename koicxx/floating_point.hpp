#ifndef KOICXX_FLOAT_HPP
#define KOICXX_FLOAT_HPP

#include <cmath>
#include <limits>

namespace koicxx {
namespace floating_point {

/**
 * \brief Rounds the specified value to the required amount of digits
 */
inline
double round(double value, int digits)
{
  const double temp = std::pow(10.0, digits);
  return std::floor(value * temp + 0.5) / temp; // std::floor(value * 10 ^ _��������_ + 0.5) / 10 ^ _��������_
}

/**
 * \brief Checks whether both arguments are equal
 */
template <typename T>
bool is_equal(T first, T second)
{
  return std::fabs(first - second) <= std::numeric_limits<T>::epsilon();
}

/**
 * \brief Check whether the first argument is less than or equal the second argument
 */
template <typename T>
bool less_than_or_equal(T left, T right)
{
  return left < right || is_equal(left, right);
}

/**
 * \brief Check whether the first argument is greater than or equal the second arguments
 */
template <typename T>
bool greater_than_or_equal(T left, T right)
{
  return left > right || is_equal(left, right);
}

} // namespace floating_point
} // namespace koicxx

#endif // !KOICXX_FLOAT_HPP