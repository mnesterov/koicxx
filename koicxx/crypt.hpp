#ifndef KOICXX_CRYPT_HPP
#define KOICXX_CRYPT_HPP

#include <koicxx/exception.hpp>
#include <koicxx/make_string.hpp>

#include <boost/scope_exit.hpp>

# ifdef _MSC_VER
#  define WIN32_LEAN_AND_MEAN
#  include <Windows.h>
#  include <Wincrypt.h>
# endif // _MSC_VER

#include <cstddef>
#include <ios>
#include <sstream>
#include <string>

namespace koicxx {
namespace crypt {

class exception : public koicxx::exception {};

enum class hash_type { SHA1, MD5, SHA256 };

#ifdef _MSC_VER

/**
 * \brief Returns a hash text
 * \throw koicxx::crypt::exception on failure
 *
 * \todo Add non-MSVC version
 */
std::string get_hash_text(const void* data, const std::size_t data_size, hash_type hash_t)
{
  HCRYPTPROV hProv = NULL;

  if (CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_AES, CRYPT_VERIFYCONTEXT) == FALSE)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function CryptAcquireContext. Error code: " << GetLastError());
  }
  BOOST_SCOPE_EXIT_ALL(hProv)
  {
    BOOST_VERIFY(
      CryptReleaseContext(hProv, 0) == TRUE
      && "An error occurred while using function CryptReleaseContext"
    );
  };

  BOOL res = FALSE;
  HCRYPTPROV hHash = NULL;
  switch (hash_t)
  {
  case hash_type::SHA1:
    res = CryptCreateHash(hProv, CALG_SHA1, 0, 0, &hHash);
    break;

  case hash_type::MD5:
    res = CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash);
    break;

  case hash_type::SHA256:
    res = CryptCreateHash(hProv, CALG_SHA_256, 0, 0, &hHash);
    break;
  }
  if (res == FALSE)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function CryptCreateHash. Error code: " << GetLastError());
  }
  BOOST_SCOPE_EXIT_ALL(hHash)
  {
    BOOST_VERIFY(
      CryptDestroyHash(hHash)
      && "An error occurred while using function CryptDestroyHash"
    );
  };

  if (CryptHashData(hHash, static_cast<const BYTE*>(data), data_size, 0) == FALSE)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function CryptHashData. Error code: " << GetLastError());
  }

  DWORD cbHashSize = 0;
  DWORD dwCount = sizeof(DWORD);
  if (CryptGetHashParam(hHash, HP_HASHSIZE, (BYTE*)&cbHashSize, &dwCount, 0) == FALSE)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function CryptGetHashParam for HP_HASHSIZE. Error code: " << GetLastError());
  }

  std::vector<BYTE> buffer(cbHashSize);
  if (CryptGetHashParam(hHash, HP_HASHVAL, reinterpret_cast<BYTE*>(&buffer[0]), &cbHashSize, 0) == FALSE)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function CryptGetHashParam for HP_HASHVAL. Error code: " << GetLastError());
  }

  std::ostringstream oss;

  for (auto itr = buffer.begin(), end = buffer.end(); itr != end; ++itr)
  {
    oss.fill('0');
    oss.width(2);
    oss << std::hex << static_cast<const int>(*itr);
  }

  return oss.str();
}

#endif // _MSC_VER

} // namespace crypt
} // namespace koicxx

#endif // !KOICXX_CRYPT_HPP