#ifndef KOICXX_STRING_HPP
#define KOICXX_STRING_HPP

# ifdef _MSC_VER
#  define _SCL_SECURE_NO_WARNINGS
#  define _CRT_SECURE_NO_WARNINGS
# endif // _MSC_VER

#include <koicxx/exception.hpp>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/trim.hpp>

#include <algorithm>
#include <cctype>
#include <cstdarg>
#include <cstdio>
#include <memory>
#include <string>

namespace koicxx {
namespace string {

class exception : public koicxx::exception {};

#ifdef _MSC_VER

/**
 * \brief Safe version of std::sprintf function. It doesn't need to know the size of the resulting array
 * \throw koicxx::string::exception object on failure
 */
inline
std::unique_ptr<char[]> safe_sprintf(const char * const fmt, ...)
{
  std::va_list args;
  va_start(args, fmt);
  int size = vsnprintf(nullptr, 0, fmt, args); // MSVC doesn't place vsnprintf function in the std namespace
  if (size < 0)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason("An error occurred while using vsnprintf function");
  }
  std::unique_ptr<char[]> buf(new char[size + 1]);

  if (std::vsprintf(buf.get(), fmt, args) < 0)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason("An error occurred while using std::vsprintf function");
  }

  va_end(args);
  return buf;
}

#elif !defined(BOOST_NO_VARIADIC_TEMPLATES) // !_MSC_VER

/**
 * \brief Safe version of std::sprintf function. It doesn't need to know the size of the resulting array
 * \throw koicxx::string::exception object on failure
 */
template <typename... Args>
std::unique_ptr<char[]> safe_sprintf(const char * const fmt, Args... args)
{
int size = std::snprintf(nullptr, 0, fmt, args...);
if (size < 0)
{
  KOICXX_THROW(exception())
    << koicxx::exception::reason("An error occurred while using std::snprintf function");
}
std::unique_ptr<char[]> buf(new char[size + 1]);

if (std::sprintf(buf.get(), fmt, args...) < 0)
{
  KOICXX_THROW(exception())
    << koicxx::exception::reason("An error occurred while using std::sprintf function");
}

return buf;
}

#endif

inline
std::string get_string_between(const std::string& input, const std::string& first, const std::string& second)
{
  const auto first_string_begin = input.find(first);
  if (first_string_begin == std::string::npos)
  {
    return std::string();
  }
  const auto first_string_end = first_string_begin + first.size();
  const auto second_string_begin = input.find(second, first_string_end);
  if (second_string_begin == std::string::npos)
  {
    return std::string();
  }
  return input.substr(first_string_end, second_string_begin - first_string_end);
}

inline
void remove_spaces(std::string& input)
{
  input.erase(
    std::remove_if(
      input.begin()
      , input.end()
      , [](char c)
      { 
        return std::isspace(static_cast<unsigned char>(c));
      }
    ), input.end()
  );
}

inline
void remove_trailing_nulls(std::string& input)
{
  boost::trim_right_if(input, [](char c){ return c == '\0'; });
}

inline
void remove_trailing_cr_and_lf_characters(std::string& input)
{
  boost::trim_right_if(input, boost::is_any_of("\r\n"));
}

} // namespace string
} // namespace koicxx

#endif // !KOICXX_STRING_HPP
