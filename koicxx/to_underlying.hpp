#ifndef KOICXX_TO_UNDERLYING_HPP
#define KOICXX_TO_UNDERLYING_HPP

#include <type_traits>

namespace koicxx {

template <typename E>
typename std::underlying_type<E>::type to_underlying(E e)
{
  return static_cast<typename std::underlying_type<E>::type>(e);
}

} // namespace koicxx

#endif // !KOICXX_TO_UNDERLYING_HPP
